//
//  BookCell.m
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "BookCell.h"
#import "UIInsetLabel.h"

#define kTitleLabelHeight 24

@implementation BookCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
		
		UIView *labelBackground = [[UIView alloc] initWithFrame:CGRectMake(0,
																		   self.frame.size.height - kTitleLabelHeight,
																		   self.frame.size.width,
																		   kTitleLabelHeight)];
		[labelBackground setBackgroundColor:[UIColor redColor]];
		
		CAGradientLayer *gradient = [CAGradientLayer layer];
		self.title = [[UIInsetLabel alloc] initWithFrame:CGRectMake(0,
															   0,
															   self.frame.size.width,
															   kTitleLabelHeight)];
		[[self title] setBackgroundColor:[UIColor clearColor]];
		[self.title setTextColor:[UIColor whiteColor]];
		[self.title setFont:[UIFont systemFontOfSize:10]];
		[self.title setRightInset:5];
		[self.title setLeftInset:5];

		gradient.frame = labelBackground.bounds;
		gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
		[labelBackground.layer insertSublayer:gradient atIndex:10];
		[labelBackground addSubview:self.title];
		[self.imageView addSubview:labelBackground];
		
		[self addSubview:self.imageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
