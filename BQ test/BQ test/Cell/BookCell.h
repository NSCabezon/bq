//
//  BookCell.h
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIInsetLabel;

@interface BookCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIInsetLabel *title;


@end
