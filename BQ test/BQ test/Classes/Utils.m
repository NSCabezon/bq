//
//  Utils.h
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "Utils.h"

@implementation Utils


+(void)saveImageToDisk:(UIImage *)img withName:(NSString *)imageName
{
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *dataPath = [[docDir stringByAppendingPathComponent:@"images"] stringByAppendingPathComponent:@"med"];
	
	NSError *error;
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
	
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@", dataPath, imageName];
	if (img) [UIImagePNGRepresentation(img) writeToFile:pngFilePath atomically:YES];
}


+(UIImage *)loadImageFromDisk:(NSString *) imageName
{
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *dataPath = [[docDir stringByAppendingPathComponent:@"images"] stringByAppendingPathComponent:@"med"];
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@", dataPath, imageName];
	NSData *retrievedData = [NSData dataWithContentsOfFile:pngFilePath];
	UIImage *image = [UIImage imageWithData:retrievedData];
	return image;
}


+(BOOL)isPad
{
	return [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
}


@end