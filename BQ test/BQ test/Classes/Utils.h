//
//  Utils.m
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface Utils : NSObject

+(void)saveImageToDisk:(UIImage *)img withName:(NSString *)imageName;
+(UIImage *)loadImageFromDisk:(NSString *)imageName;
+(BOOL)isPad;


@end