//
//  ViewController.m
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "MasterViewController.h"

#import "BookCell.h"
#import "Constants.h"
#import "DetailViewController.h"
#import "UIInsetLabel.h"
#import "Utils.h"
#import "UIImageView+Helper.h"
#import "UIView+FrameUtils.h"

#define kCellID @"cell"
#define kMargin 10
#define kItemWidth 90
#define kTitleWidth 160

@interface MasterViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *booksArray, *filteredArray;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedController;
@property (nonatomic) BOOL isFiltered, titleViewHidden, isAnimating;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIButton *titleButton, *isbnButton;
@property (weak, nonatomic) IBOutlet UIView *helpView;
@property (weak, nonatomic) IBOutlet UILabel *helpLabel;

@end

@implementation MasterViewController

-(id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
		self.booksArray = [NSArray array];
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self makePetition];
	
	self.isFiltered = NO;
	
	[self configTitleView];
	[[self navigationItem] setTitleView:self.titleView];
	
	if (![[NSUserDefaults standardUserDefaults] valueForKey:@"firstTime"])
		[self showHelpView];
	
	[self.segmentedController setTitle:NSLocalizedString(@"all", @"") forSegmentAtIndex:0];
	[self.segmentedController setTitle:NSLocalizedString(@"normal", @"") forSegmentAtIndex:1];
	[self.segmentedController setTitle:NSLocalizedString(@"premium", @"") forSegmentAtIndex:2];
	
	if (isIOS7)
		[self setEdgesForExtendedLayout:UIRectEdgeNone];
	
	[self.collectionView registerClass:[BookCell class] forCellWithReuseIdentifier:kCellID];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - CollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.isFiltered ? [self.filteredArray count] : [self.booksArray count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *object;
	if (self.isFiltered)
		object = [self.filteredArray objectAtIndex:indexPath.row];
	else
		object = [self.booksArray objectAtIndex:indexPath.row];
	
	
	BookCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
	
	[[cell title] setText:[object valueForKey:@"title"]];
	[[cell imageView] setImageWithURL:[object valueForKey:@"coverUrl"]];
	
	return cell;
}


#pragma mark - CollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier:@"detailSegue" sender:[self.booksArray objectAtIndex:indexPath.row]];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"detailSegue"])
    {
		DetailViewController *detail = [segue destinationViewController];
		[detail setBook:(NSDictionary *)sender];
    }
}


#pragma mark - LayoutDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	return CGSizeMake(kItemWidth, kItemWidth * 1.5);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 12;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(8, 8, 8, 8);
}


#pragma mark - SegmentedController
- (IBAction)segmentedChangedValue:(UISegmentedControl *) segmented
{
	switch ([segmented selectedSegmentIndex]) {
		case 0:
		{
			self.isFiltered = NO;
			[[self collectionView] reloadData];
			break;
		}
		case 1:
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isPremium = %d", YES];
			self.filteredArray = [self.booksArray filteredArrayUsingPredicate:predicate];
			self.isFiltered = YES;
			[[self collectionView] reloadData];
			break;
		}
		case 2:
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isPremium = %d", NO];
			self.filteredArray = [self.booksArray filteredArrayUsingPredicate:predicate];
			self.isFiltered = YES;
			[[self collectionView] reloadData];
			break;
		}
		default:
			break;
	}
}


#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarParam
{
	[self.searchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0)
        [self searchCollectionView:searchText];
	else
	{
		self.filteredArray = self.booksArray;
		self.isFiltered = NO;
		[[self collectionView] reloadData];
	}
}


- (void)searchCollectionView:(NSString *) searchText
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains %@", searchText];
	self.filteredArray = [self.booksArray filteredArrayUsingPredicate:predicate];
	self.isFiltered = YES;
	[[self collectionView] reloadData];
}


#pragma mark - Scrollview Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[self.searchBar resignFirstResponder];
}


#pragma mark - API methods
-(void) makePetition
{
	NSURL *url = [[NSURL alloc] initWithString:kBooksURL];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	self.booksArray = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
	self.booksArray = [[NSSet setWithArray:self.booksArray] allObjects];
}


#pragma mark - TitleView
-(void) configTitleView
{
	self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kTitleWidth, 44)];
	
	self.titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
	[self.titleButton setCenter:CGPointMake(kTitleWidth/2, self.titleButton.frame.size.height/2)];
	[self.titleButton addTarget:self action:@selector(titleAction) forControlEvents:UIControlEventTouchUpInside];
	[self.titleButton setTitle:NSLocalizedString(@"title2", @"") forState:UIControlStateNormal];
	[self.titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.titleView addSubview:self.titleButton];
	
	self.isbnButton = [[UIButton alloc] initWithFrame:CGRectMake(60, 0, 60, 44)];
	[self.titleButton setCenter:CGPointMake(kTitleWidth/2, self.titleButton.frame.size.height/2)];
	[self.isbnButton addTarget:self action:@selector(isbnAction) forControlEvents:UIControlEventTouchUpInside];
	[self.isbnButton setTitle:@"ISBN" forState:UIControlStateNormal];
	[self.isbnButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.isbnButton setHidden:YES];
	[self.titleView addSubview:self.isbnButton];
	
	[self.helpLabel setText:NSLocalizedString(@"helpText", @"")];
}


-(void) titleAction
{
	if (![[NSUserDefaults standardUserDefaults] valueForKey:@"firstTime"])
		[self showHelpView];
	
	if (!self.titleViewHidden && !self.isAnimating)
	{
		[self.isbnButton setHidden:NO];
		[UIView animateWithDuration:0.4
						 animations:^{
							 [self.titleButton frameMoveByXDelta:-self.titleButton.frame.size.width];
							 [self.isbnButton frameMoveByXDelta:self.isbnButton.frame.size.width];
						 } completion:^(BOOL finished) {
							 self.titleViewHidden = YES;
						 }];
	}
	else if (!self.isAnimating)
	{
		self.booksArray = [self.booksArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
			NSString *first = [a valueForKey:@"title"];
			NSString *second = [b valueForKey:@"title"];
			return [first compare:second];
		}];
		[[self collectionView] reloadData];
		[self closeTitleView];
	}
}


-(void) isbnAction
{
	self.booksArray = [self.booksArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
		NSString *first = [a valueForKey:@"isbn"];
		NSString *second = [b valueForKey:@"isbn"];
		return [first compare:second];
	}];
	[[self collectionView] reloadData];
	[self closeTitleView];
}


-(void) closeTitleView
{
	[UIView animateWithDuration:0.4
					 animations:^{
						 [self.titleButton frameMoveByXDelta:self.titleButton.frame.size.width];
						 [self.isbnButton frameMoveByXDelta:-self.isbnButton.frame.size.width];
					 } completion:^(BOOL finished) {
						 [self.isbnButton setHidden:YES];
						 self.titleViewHidden = NO;
					 }];
}


-(void) showHelpView
{
	[self.helpView setHidden:NO];
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelpView)];
	[self.helpView addGestureRecognizer:tap];
	[[NSUserDefaults standardUserDefaults] setValue:@"done" forKey:@"firstTime"];
}


-(void) hideHelpView
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 [self.helpView setAlpha:0];
					 }];
}



@end
