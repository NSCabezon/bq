//
//  DetailViewController.m
//  BQ test
//
//  Created by Ivan Cabezon on 31/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "DetailViewController.h"
#import "UIImageView+Helper.h"

#define kURLbase @"http://bqreader.eu01.aws.af.cm/books/"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *bookTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *isbnLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSMutableData *receivedData;

@end

@implementation DetailViewController

- (id)init
{
    self = [super init];
    if (self) {
		[[self view] setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.receivedData = [[NSMutableData alloc] init];
	[self configureBookInfo];
	[self makePetition];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)configureBookInfo
{
	[[self imageView] setImageWithURL:[self.book valueForKey:@"coverUrl"]];
	[self setTitle:[self.book valueForKey:@"title"]];
	[[self bookTitleLabel] setText:[NSLocalizedString(@"title", @"") stringByAppendingString:[self.book valueForKey:@"title"]]];
	[[self isbnLabel] setText:[NSLocalizedString(@"isbn", @"") stringByAppendingString:[self.book valueForKey:@"isbn"]]];
}

-(void) updateBookDetail
{
	[self.webView loadHTMLString:[self.book valueForKey:@"synopsis"] baseURL:nil];
}


#pragma mark - API methods
-(void) makePetition
{
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[kURLbase stringByAppendingString:[self.book valueForKeyPath:@"isbn"]]]];
	[NSURLConnection connectionWithRequest:request delegate:self];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.receivedData appendData:data];
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response {
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	if (self.receivedData)
		self.book = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:nil];
	[self updateBookDetail];
}



@end
