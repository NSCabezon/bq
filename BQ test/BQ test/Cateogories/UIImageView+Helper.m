//
//  UIImageView+Helper.m
//  Festivales
//
//  Created by Ivan Cabezon on 19/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "UIImageView+Helper.h"
#import "Utils.h"

@implementation UIImageView (UIImageView)

- (void) setImageWithURL:(NSString *)urlString
{
	UIImage *imagen = [Utils loadImageFromDisk:[urlString lastPathComponent]];
	if (!imagen) {
		NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
		NSString *dataPath = [[docDir stringByAppendingPathComponent:@"images"]
							  stringByAppendingPathComponent:@"med"];
		
		__block UIImage *imagenDL = [[UIImage alloc] init];
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul), ^{
			NSURL *imageURL = [NSURL URLWithString:urlString];
			NSData *image = [NSData dataWithContentsOfURL:imageURL];
			imagenDL = [UIImage imageWithData:image];
			if (image) [image writeToFile:[dataPath stringByAppendingPathComponent:[urlString lastPathComponent]] atomically:YES];
			dispatch_sync(dispatch_get_main_queue(), ^{
				[self setImage:imagenDL];
				[self setAlpha:0];
				[UIView beginAnimations:@"fade in" context:nil];
				[UIView setAnimationDuration:0.4];
				[self setAlpha:1.0];
				[UIView commitAnimations];
				[Utils saveImageToDisk:imagenDL withName:[urlString lastPathComponent]];
			});
		});
	} else
		[self setImage:imagen];
}


@end
