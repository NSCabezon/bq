//
//  UIInsetLabel.h
//  Festivales
//
//  Created by Ivan Cabezon on 11/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIInsetLabel : UILabel

@property (nonatomic) CGFloat topInset;
@property (nonatomic) CGFloat leftInset;
@property (nonatomic) CGFloat bottomInset;
@property (nonatomic) CGFloat rightInset;

@end
