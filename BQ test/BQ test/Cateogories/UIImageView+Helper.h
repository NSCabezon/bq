//
//  UIImageView+Helper.h
//  Festivales
//
//  Created by Ivan Cabezon on 19/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImageView (UIImageView)

- (void) setImageWithURL:(NSString *)urlString;


@end
