//
//  UIInsetLabel.m
//  Festivales
//
//  Created by Ivan Cabezon on 11/03/14.
//  Copyright (c) 2014 Ivan Cabezon. All rights reserved.
//

#import "UIInsetLabel.h"

@implementation UIInsetLabel

- (id)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame]) {
		self.leftInset = 10;
	}
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
		self.leftInset = 10;
	}
	
	return self;
}

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {self.topInset, self.leftInset, self.bottomInset, self.rightInset};
	
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}


@end
